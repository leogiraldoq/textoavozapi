<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\Text_voiceController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Ruta de registro del usuario
Route::post('/register', [AuthController::class, 'register']);
Route::post('/login',[AuthController::class,'login'])->name('login');
Route::post('/me', [AuthController::class, 'me'])->middleware('auth:sanctum');
Route::post('/textvoice', [Text_voiceController::class, 'texttovoice'])->middleware('auth:sanctum');
