<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreText_voice;
use App\Models\Text_voice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

class Text_voiceController extends Controller
{
    public function texttovoice(StoreText_voice $request){
        $texttovoice=Text_voice::create([
            'fk_users'=>$request->user()->id,
            'Texto'=>$request['Texto']
        ]);
        return $this->ApiVoiceRSS($texttovoice->Texto);
    }

    private function ApiVoiceRSS($texto){
        $NameAudio=time();
        $Audio=Http::get("http://api.voicerss.org" , [
            "key"=>"09994a75bcc142cfb157781962d4f71d",
            "hl"=>"es-mx",
            "v"=>"Jose",
            "src"=>$texto
        ]);
        Storage::disk('local')->put($NameAudio.'.wav', $Audio);
        return Storage::download($NameAudio.'.wav');
    }

}
