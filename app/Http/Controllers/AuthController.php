<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreText_voice;
use App\Http\Requests\StoreUser;
use App\Models\Text_voice;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(StoreUser $request){
        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
        $token = $user->createToken('auth_token')->plainTextToken;
        return response()->json([
            'access_token' => $token,
        ]);
    }

    public function login(Request $request)
    {
        if (!Auth::attempt($request->only('email','password'))){
            return response()->json(['message' => 'Invalid login details'], 401);
        }
        $user = User::where('email', $request['email'])->firstOrFail();
        $token = $user->createToken('auth_token')->plainTextToken;
        return response()->json([
                'access_token' => $token,
        ]);
    }

    public function me(Request $request){
        return $request->user();
    }
}

