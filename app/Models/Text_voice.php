<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Text_voice extends Model
{
    use HasFactory;
    protected $table = 'text_voices';

    /**
     * Siempre deben enviarse estos parametros obligatorias para la creación
     */
    protected $fillable = [
        'fk_users',
        'Texto'
    ];
}
