# API Texto a Voz Laravel 8

Se deben integrar con un servicio externo que permita convertir el texto plano en un
archivo de voz (ejemplo parrot de AWS); se debe guardar en una tabla el texto y la
identificación y debe retornar el archivo de voz.

## Comenzando 🚀

_Estas instrucciones le permitirán obtener una copia del proyecto en y ponerlo en funcionamiento su máquina local para propósitos de llevar a cabo la entrevista y las pruebas para la vacante._

### Instalación 🔧
_Se tiene en cuenta que la persona cuenta con conocimientos previos de laravel_
_Descargar el repositorio en la carpeta html del servidor_

```
URL del repositorio
```

_En la consola del servidor nos situamos en el directorio donde se despliega la aplicación_

```
cd /var/www/html/example-app
```
_Configuramos nuestros accesos a la base de datos en el archivo .env_

```
nano /var/www/html/example-app/.env
```
_Cambiar en el proyecto las variables por una base de datos de su servidor con los datos de acceso el usuario debe tener permisos para crear tablas y realizar las consultas de SELECT, INSERT, UPDATE, DELETE_

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=NOMBRE_BASE_DE_DATOS
DB_USERNAME=USUARIO_BASE_DE_DATOS
DB_PASSWORD=CONTRASEÑA_BASE_DE_DATOS

```
_En en la consola del servidor ejecutamos el comando migrate para crear las tablas con las que funciona la API_

```
php artisan migrate
```

_En en la consola del servidor ponemos en marcha nuestra aplicacion para las pruebas_

```
php artisan serve
```
_ Obtendremos la dirección donde puedo consumir el servicio_

```
Starting Laravel development server: http://127.0.0.1:8000
[Sat Apr 10 14:01:20 2021] Failed to listen on 127.0.0.1:8000 (reason: Address already in use)
Starting Laravel development server: http://127.0.0.1:8001
[Sat Apr 10 14:01:20 2021] Failed to listen on 127.0.0.1:8001 (reason: Address already in use)
Starting Laravel development server: <u>http://127.0.0.1:8002</u>
[Sat Apr 10 14:01:21 2021] PHP 7.4.9 Development Server (http://127.0.0.1:8002) started
[Sat Apr 10 14:01:27 2021] 127.0.0.1:40630 Accepted
[Sat Apr 10 14:01:28 2021] 127.0.0.1:40632 Accepted
[Sat Apr 10 14:01:28 2021] 127.0.0.1:40632 Closing
```

## API ⚙️
_Despues de tener nuestro servidor de desarrollo activo obtenemos la url de pruebas en este caso es http://127.0.0.1:8002

### Protocolo:
_La API solo permite llamdos POST

### Voice RSS:
_Plataforma que se utilizo para la conversión de texto a audio se consumen sus servicios por medio de una API
_www.voicerss.org/api/

### Utilización:
#### Crear usuario: Realizar un llamado a http://127.0.0.1:8002/api/register enviando los siguientes parametros POST
```
{
'name':'Nombre completo del usuario',
'email':'Correo electronico del usuario',
'password':'Contraseña para el usuario'
}
```
_Se recibe un JSON con el tocken de acceso_
```
{
"access_token": "3|DQehI0461q8VIVWCz6rRz6mkGYD37l2DjEOhSWfM"
}
```
#### Inicia Sesion: Realizar un llamado a http://127.0.0.1:8002/api/login enviando los siguientes parametros POST
```
{
'email':'Correo electronico del usuario',
'password':'Contraseña para el usuario'
}
```
_El servicio devuelve un JSON con el tocken de acceso, por favor guardar este token en un lugar seguro pues con este token se enviantodas las peticiones al servicio_
```
{
"access_token": "3|DQehI0461q8VIVWCz6rRz6mkGYD37l2DjEOhSWfM"
}
```
#### Texto a Voz: Realizar un llamado a http://127.0.0.1:8002/api/textvoice enviando los siguientes parametros POST

```
{
"Texto":"Texto Enviado"
}
```

_En los encabezados o headers debe colocar el access_token devuelto en el inicio de sesión, el token generado es de tipo Bearer por lo tanto el llamdo debe ser asi_

```
Bearer 4|jHNjaj9Ga19iGEcdXlI3R5HZ54mNWx2Yo8UlIrGc
```

_Se devolvera el archivo de audio en formato wav

## Autores

* **Leonardo Giraldo Quintero** - *Trabajo Entrevista* - *giraldoquinteroleo@gmail.com*
